package com.zjxt.background.mapper;

import java.util.List;

import com.zjxt.background.entity.TestPoint;

public interface TestPointMapper {

	int updateByPrimaryeySelective(TestPoint record);

	int deleteByPrimaryKey(Integer id);

    int insert(TestPoint record);

    int insertSelective(TestPoint record);

    TestPoint selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TestPoint record);

    int updateByPrimaryKey(TestPoint record);
    List<TestPoint> selectAll();
}