package com.zjxt.background.mapper;

import com.zjxt.background.entity.Diagnosis;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface DiagnosisMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Diagnosis record);

    int insertSelective(Diagnosis record);

    Diagnosis selectByPrimaryKey(Integer id);

    int startAll();
    int stopAll();

    int updateByPrimaryKeySelective(Diagnosis record);

    int updateByPrimaryKey(Diagnosis record);
    Diagnosis selectByMachineGroupId(Integer machineGroupId);
    List<Diagnosis> selectAll();

}