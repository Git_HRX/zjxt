package com.zjxt.background.mapper;

import java.util.List;

import com.zjxt.background.entity.Session;
import com.zjxt.background.entity.TestPoint;

public interface SessionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Session record);

    int insertSelective(Session record);

    Session selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Session record);

    int updateByPrimaryKey(Session record);
    List<Session> selectAll();
}