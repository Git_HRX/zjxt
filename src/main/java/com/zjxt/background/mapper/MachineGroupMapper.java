package com.zjxt.background.mapper;

import com.zjxt.background.entity.MachineGroup;

import java.util.List;

public interface MachineGroupMapper {
    int deleteByPrimaryKey(Integer id);
    int insert(MachineGroup record);
    int insertSelective(MachineGroup record);
    MachineGroup selectByPrimaryKey(Integer id);
    int updateByPrimaryKeySelective(MachineGroup record);
    int updateByPrimaryKey(MachineGroup record);
    List<MachineGroup> selectAll();
}