package com.zjxt.background.entity;

public class Diagnosis {
    private Integer id;

    private String status;

    private Integer machineGroupId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public Integer getMachineGroupId() {
        return machineGroupId;
    }

    public void setMachineGroupId(Integer machineGroupId) {
        this.machineGroupId = machineGroupId;
    }
}