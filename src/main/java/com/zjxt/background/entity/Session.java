/**
 * 
 */
package com.zjxt.background.entity;

/**
 * @ClassName: Session
 * @author Administrator
 * @version 2019�?4�?1�?
 */
public class Session {
	private Integer id;
	private Integer unitNum;
	private Integer sessionNum;
	private String sessionName;
	private String sessionDes;
	private Byte sessionType;
	private String sessionAnswer;
	private String dataName;
	private Integer dataNum;
	
	/**
	 * @param id
	 * @param unitNum
	 * @param sessionNum
	 * @param dataNum
	 * @param sessionName
	 * @param sessionDes
	 * @param sessionType
	 * @param sessionAnswer
	 * @param dataName
	 */
	public Session() {}
	public Session( Integer unitNum, Integer sessionNum, Integer dataNum,String sessionName, String sessionDes,
			Byte sessionType, String sessionAnswer, String dataName) {
		super();
		this.unitNum = unitNum;
		this.sessionNum = sessionNum;
		this.dataNum=dataNum;
		this.sessionName = sessionName;
		this.sessionDes = sessionDes;
		this.sessionType = sessionType;
		this.sessionAnswer = sessionAnswer;
		this.dataName = dataName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUnitNum() {
		return unitNum;
	}
	public void setUnitNum(Integer unitNum) {
		this.unitNum = unitNum;
	}
	public Integer getSessionNum() {
		return sessionNum;
	}
	public void setSessionNum(Integer sessionNum) {
		this.sessionNum = sessionNum;
	}
	public Integer getDataNum() {
		return dataNum;
	}
	public void setDataNum(Integer dataNum) {
		this.dataNum = dataNum;
	}
	public String getSessionName() {
		return sessionName;
	}
	public void setSessionName(String sessionName) {
		this.sessionName = sessionName == null ? null : sessionName.trim();
	}
	public String getSessionDes() {
		return sessionDes;
	}
	public void setSessionDes(String sessionDes) {
		this.sessionDes = sessionDes == null ? null : sessionDes.trim();
	}
	public Byte getSessionType() {
		return sessionType;
	}
	public void setSessionType(Byte sessionType) {
		this.sessionType = sessionType;
	}
	public String getSessionAnswer() {
		return sessionAnswer;
	}
	public void setSessionAnswer(String sessionAnswer) {
		this.sessionAnswer = sessionAnswer == null ? null : sessionAnswer.trim();
	}
	public String getDataName() {
		return dataName;
	}
	public void setDataName(String dataName) {
		this.dataName = dataName == null ? null : dataName.trim();
	}
	
}
