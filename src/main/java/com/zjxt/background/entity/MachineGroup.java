package com.zjxt.background.entity;

public class MachineGroup {
    private Integer id;

    private Integer jizuStatus;

    @Override
	public String toString() {
		return "MachineGroup [id=" + id + ", jizuStatus=" + jizuStatus + ", jiziNum=" + jiziNum + ", jizuName="
				+ jizuName + ", jizuDes=" + jizuDes + "]";
	}

	private Integer jiziNum;

    private String jizuName;

    private String jizuDes;
    public MachineGroup(Integer jizuStatus,Integer jizuNum,String jizuName,String jizuDes){
    	this.jizuStatus=jizuStatus;
    	this.jiziNum=jizuNum;
    	this.jizuName=jizuName;
    	this.jizuDes=jizuDes;
    }
    public MachineGroup() {}
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getJizuStatus() {
        return jizuStatus;
    }

    public void setJizuStatus(Integer jizuStatus) {
        this.jizuStatus = jizuStatus;
    }

    public Integer getJiziNum() {
        return jiziNum;
    }

    public void setJiziNum(Integer jiziNum) {
        this.jiziNum = jiziNum;
    }

    public String getJizuName() {
        return jizuName;
    }

    public void setJizuName(String jizuName) {
        this.jizuName = jizuName == null ? null : jizuName.trim();
    }

    public String getJizuDes() {
        return jizuDes;
    }

    public void setJizuDes(String jizuDes) {
        this.jizuDes = jizuDes == null ? null : jizuDes.trim();
    }
}