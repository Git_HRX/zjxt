package com.zjxt.background.entity;

import java.util.Date;

public class DiagnosisHistory {
    private Integer id;

    private Date time;

    private Integer machineGroupId;

    private Integer problemId;

    private String probability;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getMachineGroupId() {
        return machineGroupId;
    }

    public void setMachineGroupId(Integer machineGroupId) {
        this.machineGroupId = machineGroupId;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public String getProbability() {
        return probability;
    }

    public void setProbability(String probability) {
        this.probability = probability == null ? null : probability.trim();
    }
}