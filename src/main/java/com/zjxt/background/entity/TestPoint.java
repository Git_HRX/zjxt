package com.zjxt.background.entity;

public class TestPoint {

	private Integer id;

    private Integer unitNum;

    private Integer pointNum;

    private String pointName;

    private String pointDes;
    public TestPoint() {}
    public TestPoint(Integer unitNum,Integer pointNum,String pointName,String pointDes) {
    	this.unitNum=unitNum;
    	this.pointDes=pointDes;
    	this.pointName=pointName;
    	this.pointNum=pointNum;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUnitNum() {
        return unitNum;
    }

    public void setUnitNum(Integer unitNum) {
        this.unitNum = unitNum;
    }

    public Integer getPointNum() {
        return pointNum;
    }

    public void setPointNum(Integer pointNum) {
        this.pointNum = pointNum;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName == null ? null : pointName.trim();
    }

    public String getPointDes() {
        return pointDes;
    }

    public void setPointDes(String pointDes) {
        this.pointDes = pointDes == null ? null : pointDes.trim();
    }
}