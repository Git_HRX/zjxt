package com.zjxt.background.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zjxt.background.entity.MachineGroup;
import com.zjxt.background.entity.SysRole;
import com.zjxt.background.entity.SysUser;
import com.zjxt.background.mapper.MachineGroupMapper;
	
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zjxt.background.entity.MachineGroup;

import java.util.List;

/**
 * @ClassName: MachineGroupController
 * @Description: TODO
 * @Author: HRX
 * @Date: 2019/3/16 12:48
 **/
@Controller
@RequestMapping("/machineGroup")
public class MachineGroupController {
	@Autowired
	private MachineGroupMapper machineGroupMapper;
	
	/* 列表显示*/
	@RequestMapping(value = { "", "/" })
	public String index(Model model,
			@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,
			@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize) {
		PageHelper.startPage(pageNo, pageSize);
		List<MachineGroup> list = machineGroupMapper.selectAll();
		// 一个一个将角色资源列表加入
		PageInfo<MachineGroup> pageInfo = new PageInfo<MachineGroup>(list);
		if (pageNo > pageInfo.getPages()) {
			pageNo = pageInfo.getPages();
			PageHelper.startPage(pageNo, pageSize);
			list = machineGroupMapper.selectAll();
			pageInfo = new PageInfo<MachineGroup>(list);
		}
		model.addAttribute("pageInfo", pageInfo);
		return "admin/machine/list";
	}
	/*机组增加页面跳转*/
	@RequestMapping(value = "/add-machine")
    public String addMachineBefore(){
    		return "admin/machine/add";
    }
	/*机组增加保存*/
	
	@RequestMapping(value = "/add-machine",params="save=true")
	@ResponseBody
	public String addMachineAfter(Model model,@RequestParam(defaultValue="0") Integer jizuStatus,Integer jiziNum,String jizuName,String jizuDes) {
		boolean flag=false;
		MachineGroup machineGroup=new MachineGroup(jizuStatus,jiziNum,jizuName,jizuDes);
		machineGroupMapper.insert(machineGroup);
		flag=true;
		model.addAttribute("isSuccess", flag);
		return "admin/machine/add";
	}
	/*机组编辑页面跳转*/
	
	@RequestMapping(value = "/edit-machine/{id}")
	public String editMachineBefore(Model model,@ModelAttribute(value = "machine") MachineGroup machine,@PathVariable("id") Integer id) {
		MachineGroup machineGroup=new MachineGroup();
		machineGroup.setId(id);
		machineGroup=machineGroupMapper.selectByPrimaryKey(id);
		model.addAttribute("machine",machineGroup);
		return "admin/machine/edit";
	}
	/*机组编辑保存*/
	@RequestMapping(value = "/edit-machine",params="save=false")
	@ResponseBody
	public String editMachineAfter(Model model,Integer id ,@RequestParam(defaultValue="0") Integer jizuStatus,Integer jiziNum,String jizuName,String jizuDes) {                                                                                                                           
		MachineGroup machineGroup=new MachineGroup(jizuStatus,jiziNum,jizuName,jizuDes); 
		machineGroup.setId(id);
		machineGroupMapper.updateByPrimaryKey(machineGroup);  
		//机组编辑保存和机组添加保存是一样的
		return "admin/machine/add";                                                                                                                  
	}                                                                                                                                                
	/*删除一个*/
	@RequestMapping(value = "/delete-one")
	@ResponseBody
	public String deleteOne(Model model,@RequestParam(value="id",defaultValue="1") Integer id) {
		machineGroupMapper.deleteByPrimaryKey(id);
		return "1";
	}
	/*删除多个*/
	@ResponseBody
	@RequestMapping(value = "/deleteAll", produces = "application/json", consumes = "application/json")
	public Integer deleteAll(@RequestBody Integer[] ids) {
		try {
			for (Integer id : ids) {
				machineGroupMapper.deleteByPrimaryKey(id);
			}
			return 1;
		} catch (Exception e) {
			//logger.error(e.getMessage());
		}
		return 0;
	}
}
