package com.zjxt.background.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zjxt.background.entity.Diagnosis;
import com.zjxt.background.entity.Problem;
import com.zjxt.background.mapper.ProblemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: HRX
 * @Date: 2019/4/6 21:03
 * @Description:
 */
@Controller
@RequestMapping("problem")
public class ProblemController {
    @Autowired
    private ProblemMapper problemMapper;

    String prefix = "/admin/knowledge/problems/";

    /* 列表显示*/
    @RequestMapping(value = { "", "/" })
    public String index(Model model,
                        @RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,
                        @RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize,
                        @ModelAttribute Problem problem) {
        PageHelper.startPage(pageNo, pageSize);
        List<Problem> list = problemMapper.selectByProblem(problem);
        PageInfo<Problem> pageInfo = new PageInfo<Problem>(list);
        if (pageNo > pageInfo.getPages()) {
            pageNo = pageInfo.getPages();
            PageHelper.startPage(pageNo, pageSize);
            list = problemMapper.selectByProblem(problem);
            pageInfo = new PageInfo<>(list);
        }
        model.addAttribute("pageInfo", pageInfo);
        return prefix+"list";
    }


    @RequestMapping(value = "/beforeAdd")
    public String addRoleBefore(Model model) {
        return prefix+"add";
    }
}
