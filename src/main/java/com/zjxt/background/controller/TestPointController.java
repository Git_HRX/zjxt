/**
 * 
 */
package com.zjxt.background.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zjxt.background.entity.TestPoint;
import com.zjxt.background.mapper.TestPointMapper;

import ch.qos.logback.core.net.SyslogOutputStream;

/**
 * @ClassName: TestPointController
 * @author Administrator
 * @version 2019年3月19日
 */
@Controller
@RequestMapping("/testPoint")
public class TestPointController {
	@Autowired
	private TestPointMapper TestPointMapper;
	
	/* 列表显示*/
	@RequestMapping(value = { "", "/" })
	public String index(Model model,
			@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,
			@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize) {
		PageHelper.startPage(pageNo, pageSize);
		List<TestPoint> list = TestPointMapper.selectAll();
		// 一个一个将角色资源列表加入
		System.out.println("列表显示成功");
		PageInfo<TestPoint> pageInfo = new PageInfo<TestPoint>(list);
		if (pageNo > pageInfo.getPages()) {
			pageNo = pageInfo.getPages();
			PageHelper.startPage(pageNo, pageSize);
			list = TestPointMapper.selectAll();
			pageInfo = new PageInfo<TestPoint>(list);
		}
		model.addAttribute("pageInfo", pageInfo);
		return "admin/testPoint/list";
	}
	/*测点增加页面跳转*/
	@RequestMapping(value = "/add-meaPoint")
    public String addtestPointBefore(){
		System.out.println("页面跳转成功");
    		return "admin/testPoint/add";
    }
	/*测点增加保存*/                
	
	@RequestMapping(value = "/add-meaPoint",params="save=true")
	@ResponseBody
	public String addtestPointAfter(Model model,@RequestParam(defaultValue="1")Integer unitNum,Integer pointNum,String pointName,String pointDes) {
		System.out.println("页面保存成功");
		TestPoint TestPoint=new TestPoint(pointNum,pointNum,pointName,pointDes);
		TestPointMapper.insert(TestPoint);
		return "admin/testPoint/add";
	}
	/*测点编辑页面跳转*/
	
	@RequestMapping(value = "/edit-meaPoint/{id}")
	public String edittestPointBefore(Model model,@ModelAttribute(value = "testPoint") TestPoint testPoint,@PathVariable("id") Integer id) {
		TestPoint TestPoint=new TestPoint();
		TestPoint=TestPointMapper.selectByPrimaryKey(id);
		TestPoint.setId(id);
		model.addAttribute("testPoint",TestPoint);
		return "admin/testPoint/edit";
	}
	/*测点编辑保存*/
	@RequestMapping(value = "/edit-meaPoint",params="save=false")
	@ResponseBody
	public String edittestPointAfter(Model model,Integer id ,@RequestParam(defaultValue="1") Integer unitNum ,Integer pointNum,String pointName,String pointDes) {                                                                                                                           
		TestPoint TestPoint=new TestPoint(unitNum,pointNum,pointName,pointDes); 
		TestPoint.setId(id);
		TestPointMapper.updateByPrimaryKey(TestPoint);  
		//测点编辑保存和测点添加保存是一样的
		return "admin/testPoint/add";                                                                                                                  
	}                                                                                                                                                
	/*删除一个*/
	@RequestMapping(value = "/delete-one")
	@ResponseBody
	public String deleteOne(Model model,@RequestParam(value="id",defaultValue="1") Integer id) {
		TestPointMapper.deleteByPrimaryKey(id);
		return "1";
	}
	/*删除多个*/
	@ResponseBody
	@RequestMapping(value = "/deleteAll", produces = "application/json", consumes = "application/json")
	public Integer deleteAll(@RequestBody Integer[] ids) {
		try {
			for (Integer id : ids) {
				TestPointMapper.deleteByPrimaryKey(id);
			}
			return 1;
		} catch (Exception e) {
			//logger.error(e.getMessage());
		}
		return 0;
	}
}
