/**
 * 
 */
package com.zjxt.background.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zjxt.background.entity.Session;
import com.zjxt.background.mapper.SessionMapper;

/**
 * @ClassName: SessionController
 * @author Administrator
 * @version 2019年4月1日
 */
@Controller
@RequestMapping("/Session")
public class SessionController {
	@Autowired
	private SessionMapper SessionMapper;
	
	/* 列表显示*/
	@RequestMapping(value = { "", "/" })
	public String index(Model model,
			@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,
			@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize) {
		PageHelper.startPage(pageNo, pageSize);
		List<Session> list = SessionMapper.selectAll();
		// 一个一个将角色资源列表加入
		System.out.println("列表显示成功");
		PageInfo<Session> pageInfo = new PageInfo<Session>(list);
		if (pageNo > pageInfo.getPages()) {
			pageNo = pageInfo.getPages();
			PageHelper.startPage(pageNo, pageSize);
			list = SessionMapper.selectAll();
			pageInfo = new PageInfo<Session>(list);
		}
		model.addAttribute("pageInfo", pageInfo);
		return "admin/session/list";
	}
	/*会话增加页面跳转*/
	@RequestMapping(value = "/add-Session")
    public String addSessionBefore(){
    		return "admin/session/add";
    }
	/*会话增加保存*/                
	
	@RequestMapping(value = "/add-Session",params="save=true")
	@ResponseBody
	public String addSessionAfter(Model model,@RequestParam(defaultValue="1") Integer unitNum, Integer sessionNum, Integer dataNum,String sessionName, String sessionDes,
			Byte sessionType, String sessionAnswer, String dataName) {
		System.out.println("页面保存成功"+dataNum);
		Session Session=new Session(unitNum,sessionNum,dataNum,sessionName,sessionDes,sessionType,sessionAnswer,dataName);
		System.out.println(Session.getDataNum());
		SessionMapper.insert(Session);
		return "admin/session/add";
	}
	/*会话编辑页面跳转*/
	
	@RequestMapping(value = "/edit-Session/{id}")
	public String editSessionBefore(Model model,@ModelAttribute(value = "Session") Session Session,@PathVariable("id") Integer id) {
		Session Session1=new Session();
		Session1=SessionMapper.selectByPrimaryKey(id);
		Session1.setId(id);
		model.addAttribute("session",Session1);
		return "admin/session/edit";
	}
	/*会话编辑保存*/
	@RequestMapping(value = "/edit-Session",params="save=false")
	@ResponseBody
	public String editSessionAfter(Model model,Integer id,@RequestParam(defaultValue="1") Integer unitNum, Integer sessionNum, Integer dataNum,String sessionName, String sessionDes,                                                                                                                           
			Byte sessionType, String sessionAnswer, String dataName) {                                                                                                       
		System.out.println("页面保存成功"+dataNum);                                                                                                                                
		Session Session=new Session(unitNum,sessionNum,dataNum,sessionName,sessionDes,sessionType,sessionAnswer,dataName);
		Session.setId(id);
        SessionMapper.updateByPrimaryKey(Session);
		return "admin/session/add";                                                                                                                  
	}                                                                                                                                                
	/*删除一个*/
	@RequestMapping(value = "/delete-one")
	@ResponseBody
	public String deleteOne(Model model,@RequestParam(value="id",defaultValue="1") Integer id) {
		System.out.println("删除一个");
		SessionMapper.deleteByPrimaryKey(id);
		return "1";
	}
	/*删除多个*/
	@ResponseBody
	@RequestMapping(value = "/deleteAll", produces = "application/json", consumes = "application/json")
	public Integer deleteAll(@RequestBody Integer[] ids) {
		try {
			for (Integer id : ids) {
				SessionMapper.deleteByPrimaryKey(id);
			}
			return 1;
		} catch (Exception e) {
			//logger.error(e.getMessage());
		}
		return 0;
	}
}
