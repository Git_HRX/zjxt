package com.zjxt.background.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zjxt.background.entity.Diagnosis;
import com.zjxt.background.entity.MachineGroup;
import com.zjxt.background.mapper.DiagnosisMapper;
import com.zjxt.background.mapper.MachineGroupMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("diagnosis")
public class DiagnosisController {
    @Autowired
    DiagnosisMapper diagnosisMapper;
    @Autowired
    MachineGroupMapper machineGroupMapper;


    /* 列表显示*/
    @RequestMapping(value = {"", "/"})
    public String index(Model model,
                        @RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,
                        @RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<Diagnosis> diagnosisList = diagnosisMapper.selectAll();
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        for (Diagnosis diagnosis : diagnosisList) {
            Map<String, Object> map = new HashMap<>();
            map.put("machineGroup", machineGroupMapper.selectByPrimaryKey(diagnosis.getMachineGroupId()));
            map.put("status", diagnosis.getStatus());
            map.put("id", diagnosis.getId());
            list.add(map);
        }
        if (diagnosisList.size() != machineGroupMapper.selectAll().size())
            machineGroupMapper.selectAll().forEach(machineGroup -> {
                if (diagnosisMapper.selectByMachineGroupId(machineGroup.getId()) != null) {
                    Diagnosis diagnosis = new Diagnosis();
                    diagnosis.setStatus("未启动");
                    diagnosis.setMachineGroupId(machineGroup.getId());
                    diagnosisMapper.insert(diagnosis);
                }
            });
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(list);
        if (pageNo > pageInfo.getPages()) {
            pageNo = pageInfo.getPages();
            PageHelper.startPage(pageNo, pageSize);
            list = new ArrayList<Map<String, Object>>();
            for (Diagnosis diagnosis : diagnosisList) {
                Map<String, Object> map = new HashMap<>();
                map.put("machineGroup", machineGroupMapper.selectByPrimaryKey(diagnosis.getMachineGroupId()));
                map.put("status", diagnosis.getStatus());
                map.put("id", diagnosis.getId());
                list.add(map);
            }
            pageInfo = new PageInfo<>(list);
        }
        model.addAttribute("pageInfo", pageInfo);
        return "admin/diagnosis/list";
    }

    /*机组增加页面跳转*/
    @RequestMapping(value = "/add-diagnosis")
    public String addMachineBefore() {
        return "admin/diagnosis/add";
    }
    /*机组增加保存*/

    @RequestMapping(value = "/add-diagnosis", params = "save=true")
    @ResponseBody
    public String addMachineAfter(Model model, Diagnosis diagnosis) {
        boolean flag = false;
        diagnosisMapper.insert(diagnosis);
        flag = true;
        model.addAttribute("isSuccess", flag);
        return "admin/machine/add";
    }
    /*机组编辑页面跳转*/

    @RequestMapping(value = "/edit-diagnosis/{id}")
    public String editMachineBefore(Model model, @ModelAttribute(value = "diagnosis") Diagnosis diagnosis, @PathVariable("id") Integer id) {
        Diagnosis diagnosisUpdate = diagnosisMapper.selectByPrimaryKey(id);
        model.addAttribute("machine", diagnosisUpdate);
        return "admin/machine/edit";
    }

    /*诊断编辑保存*/
    @RequestMapping(value = "/edit-diagnosis", params = "save=false")
    @ResponseBody
    public String editMachineAfter(Model model, Diagnosis diagnosis) {
        diagnosisMapper.updateByPrimaryKey(diagnosis);
        return "admin/diagnosis/add";
    }

    @RequestMapping(value = "/start-all")
    @ResponseBody
    public String startAll() {
        diagnosisMapper.startAll();
        return "ok";
    }

    @RequestMapping(value = "/change-one")
    @ResponseBody
    public String startOne(Integer id, String status) {
        Diagnosis diagnosis = diagnosisMapper.selectByPrimaryKey(id);
        diagnosis.setStatus(status);
        diagnosisMapper.updateByPrimaryKey(diagnosis);
        return "ok";
    }

    @RequestMapping(value = "/stop-all")
    @ResponseBody
    public String stopAll() {
        diagnosisMapper.stopAll();
        return "ok";
    }


    /*删除一个*/
    @RequestMapping(value = "/delete-one")
    @ResponseBody
    public String deleteOne(Model model, @RequestParam(value = "id", defaultValue = "1") Integer id) {
        diagnosisMapper.deleteByPrimaryKey(id);
        return "1";
    }

    /*删除多个*/
    @ResponseBody
    @RequestMapping(value = "/deleteAll", produces = "application/json", consumes = "application/json")
    public Integer deleteAll(@RequestBody Integer[] ids) {
        try {
            for (Integer id : ids) {
                diagnosisMapper.deleteByPrimaryKey(id);
            }
            return 1;
        } catch (Exception e) {
        }
        return 0;
    }
}
