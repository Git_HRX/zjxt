/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 5.5.62-0ubuntu0.14.04.1 : Database - background
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`background` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `background`;

/*Table structure for table `authority` */

DROP TABLE IF EXISTS `authority`;

CREATE TABLE `authority` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `code` int(100) DEFAULT NULL COMMENT '权限编号',
  `name` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '权限类别',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='权限';

/*Data for the table `authority` */

/*Table structure for table `drawback_list` */

DROP TABLE IF EXISTS `drawback_list`;

CREATE TABLE `drawback_list` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '退货退款id',
  `user_id` bigint(100) DEFAULT NULL COMMENT '销售人员id',
  `purchase_id` bigint(100) DEFAULT NULL COMMENT '采购订单id',
  `logistics_num` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '退货退款物流单号',
  `logistics_company` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '退货退款物流公司',
  `carriage_price` decimal(65,2) DEFAULT NULL COMMENT '退货退款物流价格',
  `time` date DEFAULT NULL COMMENT '退货退款时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='退货退款记录表';

/*Data for the table `drawback_list` */

insert  into `drawback_list`(`id`,`user_id`,`purchase_id`,`logistics_num`,`logistics_company`,`carriage_price`,`time`) values 
(16,1,218,'0','0',0.00,'2019-02-23');

/*Table structure for table `materiel` */

DROP TABLE IF EXISTS `materiel`;

CREATE TABLE `materiel` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '物料id',
  `code` int(100) DEFAULT NULL COMMENT '物料编号',
  `materiel_name` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '物料名称',
  `materiel_price` decimal(65,2) DEFAULT NULL COMMENT '物料价格',
  `materiel_num` int(100) DEFAULT NULL COMMENT '物料数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='物料信息';

/*Data for the table `materiel` */

insert  into `materiel`(`id`,`code`,`materiel_name`,`materiel_price`,`materiel_num`) values 
(47,1001,'外屏玻璃',20.00,100),
(48,1002,'iphone7后盖',85.00,100),
(49,1003,'iphone7P后盖',100.00,99),
(50,1004,'iphone6s Plus后盖（玫瑰金）',67.50,1);

/*Table structure for table `order_list` */

DROP TABLE IF EXISTS `order_list`;

CREATE TABLE `order_list` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '采购id',
  `user_id` bigint(100) DEFAULT NULL COMMENT '采购登记的用户id',
  `product_id` bigint(100) DEFAULT NULL COMMENT '商品类型id',
  `color` int(100) DEFAULT NULL COMMENT '商品颜色',
  `order_num` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '物流单号',
  `purchase_method` int(100) DEFAULT NULL COMMENT '采购方式',
  `order_state` int(100) DEFAULT NULL COMMENT '订单状态',
  `operator` varchar(20) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '经办人',
  `purchase_time` date DEFAULT NULL COMMENT '采购时间',
  `final_time` date DEFAULT NULL COMMENT '最终收购时间',
  `customer_name` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '客户名称',
  `serial_num` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '序列号',
  `guarantee` int(100) DEFAULT NULL COMMENT '保修',
  `apperance` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '外观',
  `random_attachment` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT '' COMMENT '随机附件',
  `purchase_price` decimal(65,2) DEFAULT NULL COMMENT '采购价格',
  `final_purchase` decimal(65,2) DEFAULT NULL COMMENT '最终收购价格',
  `quality_price` decimal(65,2) DEFAULT NULL COMMENT '最终质检价格',
  `part_price` decimal(65,2) DEFAULT NULL COMMENT '退款金额',
  `pur_remarks` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '采购人员备注',
  `qu_remark` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '质检人员备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=222 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='订单表';

/*Data for the table `order_list` */

insert  into `order_list`(`id`,`user_id`,`product_id`,`color`,`order_num`,`purchase_method`,`order_state`,`operator`,`purchase_time`,`final_time`,`customer_name`,`serial_num`,`guarantee`,`apperance`,`random_attachment`,`purchase_price`,`final_purchase`,`quality_price`,`part_price`,`pur_remarks`,`qu_remark`) values 
(195,1,103,4,'256673774717',1,4,'admin','2019-02-09','2019-02-09','源源桑很好','',2,'1,3',NULL,2001.00,1900.00,2000.00,NULL,'卖家描述:底部轻微磕碰，无拆无修，所有功能正常\n质检结果：美版，屏幕有划痕，贴膜不可见',NULL),
(196,30,104,4,'362171543801',1,0,'zhujixing','2019-02-08','2019-02-08','vian0126','',2,'2,4',NULL,2322.00,2322.00,2400.00,NULL,'在苹果官方换过摄像头 屏幕 屏幕有划痕 机身有磕碰',NULL),
(197,30,99,1,'348130873156',1,3,'zhujixing','2019-02-09','2019-02-09','远见卓识的我','',2,'1,4',NULL,1220.00,1220.00,1220.00,NULL,'换过外屏 要换外壳\n质检：屏幕严重划痕，外壳严重磕碰，距离感应器失效，内部屏幕无挡板\n\n更换屏幕总成+外壳',NULL),
(198,30,104,4,'803364816779',1,4,'zhujixing','2019-02-09','2019-02-09','人傻钱多买买买2017','',2,'1,3',NULL,2300.00,2300.00,2580.00,NULL,'无拆无修\n验机：无拆无修 状态良好，带原装盒子数据线充电器\n转转平台已售出，发至验机中心',NULL),
(200,30,104,1,'803407823329',1,8,'zhujixing','2019-02-10','2019-02-10','我爱杜银飞','',2,'1,4',NULL,2300.00,2300.00,2580.00,NULL,'无拆无修  要换壳  其它都ok\n已更换外壳（维修费100）\n收购成本2300 维修100元',NULL),
(201,1,103,4,'803371995571',1,4,'admin','2019-02-11','2019-02-11','小果冻的爱','',2,'1,3',NULL,2000.00,2000.00,2200.00,NULL,'17年四月的 95新',NULL),
(202,30,107,5,'669672567904',1,4,'zhujixing','2019-02-11','2019-02-11','大陆阔总','',2,'4',NULL,2800.00,2500.00,2900.00,NULL,'换过外屏和电池 外壳要换\n质检完成  组装屏+翻新电池',NULL),
(203,1,105,8,'036498253159',1,8,'admin','2019-02-12','2019-02-12','dgreamy','',1,'1,3','4',2200.00,2200.00,2900.00,NULL,'有发票 无磕碰  \n在保200多天',NULL),
(204,1,98,6,'036498253159',1,4,'admin','2019-02-12','2019-02-12','dgreamy','',2,'4',NULL,800.00,800.00,1000.00,NULL,'换过听筒和充电口',NULL),
(208,1,97,2,'609240040434',1,4,'admin','2019-02-13','2019-02-13','伤心的花卷','',2,NULL,NULL,850.00,850.00,950.00,NULL,'',NULL),
(209,1,94,2,'669674091072',1,4,'admin','2019-02-13','2019-02-13','androwberg','',2,'1,3',NULL,850.00,850.00,1000.00,NULL,'换过屏幕',NULL),
(210,1,101,3,'803375061213',1,4,'admin','2019-02-13','2019-02-13','水平巧','',2,NULL,NULL,1270.00,1270.00,1400.00,NULL,'10系统，需要换壳',NULL),
(212,1,103,2,'7152106061376',1,3,'admin','2019-02-13','2019-02-13','落仙塘vs男神','',2,NULL,NULL,2000.00,2000.00,2000.00,NULL,'换过电池',NULL),
(213,30,105,6,'803427632045',1,3,'zhujixing','2019-02-13','2019-02-13','zx7071625','',2,'3',NULL,2408.00,2408.00,2408.00,NULL,'外壳有小磕碰',NULL),
(214,30,101,4,'803386944835',1,4,'zhujixing','2019-02-14','2019-02-14','小小唐家小子','',2,'1,3',NULL,1200.00,1200.00,1382.00,NULL,'更换听筒 运费是到付 15',NULL),
(218,30,104,4,'362171543801',1,6,'zhujixing','2019-02-08','2019-02-08','vian0126','',2,'2,4',NULL,2322.00,2322.00,2400.00,NULL,'在苹果官方换过摄像头 屏幕 屏幕有划痕 机身有磕碰',NULL),
(219,30,101,2,'803101141627',1,1,'zhujixing','2019-02-24','2019-02-24','tb188977155','',2,'2',NULL,1100.00,NULL,NULL,NULL,'要换外屏和电池',NULL),
(221,30,100,4,'',1,9,'zhujixing','2019-02-26','2019-02-26','xin那个土斤','',2,'4',NULL,1100.00,NULL,NULL,NULL,'要换壳',NULL);

/*Table structure for table `order_new` */

DROP TABLE IF EXISTS `order_new`;

CREATE TABLE `order_new` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT COMMENT '新订单管理表id',
  `num` varchar(50) DEFAULT NULL COMMENT '新订单编号',
  `date` date DEFAULT NULL COMMENT '日期',
  `model` varchar(50) DEFAULT NULL COMMENT '型号',
  `color` varchar(50) DEFAULT NULL COMMENT '颜色',
  `version` varchar(50) DEFAULT NULL COMMENT '版本',
  `memory` varchar(50) DEFAULT NULL COMMENT '内存',
  `seller` varchar(50) DEFAULT NULL COMMENT '卖家账号',
  `express` varchar(50) DEFAULT NULL COMMENT '快递单号',
  `purchase_price` decimal(65,2) DEFAULT NULL COMMENT '收购价格',
  `purchase_express` decimal(65,2) DEFAULT NULL COMMENT '收购运费',
  `repair_price` decimal(65,2) DEFAULT NULL COMMENT '维修价格',
  `sell_price` decimal(65,2) DEFAULT NULL COMMENT '出售价格',
  `sell_express` decimal(65,2) DEFAULT NULL COMMENT '出售运费',
  `profit` decimal(65,2) DEFAULT NULL COMMENT '利润',
  `buyer` varchar(50) DEFAULT NULL COMMENT '买家账号',
  `sell_number` varchar(50) DEFAULT NULL COMMENT '售出快递单号',
  `state` varchar(50) DEFAULT NULL COMMENT '交易状态',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `user` varchar(50) DEFAULT NULL COMMENT '登记员',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COMMENT='客户新增，与其他表独立出来订单录入的表';

/*Data for the table `order_new` */

insert  into `order_new`(`id`,`num`,`date`,`model`,`color`,`version`,`memory`,`seller`,`express`,`purchase_price`,`purchase_express`,`repair_price`,`sell_price`,`sell_express`,`profit`,`buyer`,`sell_number`,`state`,`remark`,`user`) values 
(25,'C1001','2019-02-26','iphonex 256','白色','shinem86',NULL,'国行','467799398626',4800.00,NULL,NULL,NULL,NULL,-4800.00,'','','未签收','','admin'),
(26,'C1002','2019-02-26','iphonex 64','黑色','shinem86',NULL,'国行','467799398626',4300.00,NULL,NULL,NULL,NULL,-4300.00,'','','未签收','','admin'),
(27,'C1003','2019-02-26','iphonex 64','黑色','shinem86',NULL,'国行','467799398626',3900.00,NULL,NULL,NULL,NULL,-3900.00,'','','未签收','小碎','admin'),
(28,'C1004','2019-02-26','iphonex 256','黑色','shinem86',NULL,'欧版（两网）','467799398626',4650.00,NULL,NULL,NULL,NULL,-4650.00,'','','未签收','','admin'),
(29,'C1005','2019-02-26','iphone8 64','黑色','shinem86',NULL,'国行','467799398626',2550.00,NULL,NULL,NULL,NULL,-2550.00,'','','未签收','','admin'),
(30,'C1006','2019-02-26','iphonexr 64','黑色','shinem86',NULL,'英版（两网）','467799398626',4000.00,NULL,NULL,NULL,NULL,-4000.00,'','','未签收','','admin'),
(31,'C1007','2019-02-26','iphonex 256','白色','shinem86',NULL,'国行','467799398626',4300.00,NULL,NULL,NULL,NULL,-4300.00,'','','未签收','换屏换后盖','admin'),
(32,'L1001','2019-02-26','iphone8 64','白色','曹双燕8899',NULL,'国行','',2300.00,0.00,NULL,NULL,NULL,-2300.00,'','','未签收','修过基带','luodachun'),
(33,'L1002','2019-02-26','iphone8 64','金色','vin唯美',NULL,'国行','290246923327',2100.00,NULL,NULL,NULL,NULL,-2100.00,'','','未签收','换过后壳玻璃，然后又裂开了','luodachun'),
(35,'     Z1001','2019-02-26','iphone7 32 ','       粉色','     国行',NULL,'         心算','',1250.00,NULL,NULL,NULL,NULL,-1250.00,'','','未签收','手机套 边框有一次磕碰','zhangxiaodong'),
(36,'     Z1002','2019-02-26','iphone7plus128','        磨砂黑','       国行',NULL,'         杜子V','363224766207',2300.00,NULL,NULL,NULL,NULL,-2300.00,'','','未签收','原装 充电头','zhangxiaodong'),
(37,'Z1003','2019-02-26','iphone732',' 粉色','国行',NULL,'TP187799249','',1330.00,NULL,NULL,NULL,NULL,-1330.00,'','','未签收','运费到付盒子 保修卡','zhangxiaodong'),
(38,'Z1004','2019-02-26','iphone732','黑色','国行',NULL,' JJ小表弟','804569772998274025',1350.00,NULL,NULL,NULL,NULL,-1350.00,'','','未签收','','zhangxiaodong'),
(39,'L1003','2019-02-26','iphone7 plus 32','金色','国行',NULL,'tbn1779109','438785627018',1900.00,NULL,NULL,NULL,NULL,-1900.00,'','','未签收','运费到付，裸机全原','luodachun'),
(40,'C1008','2019-02-26','iphone6 Plus 64','金色','国行',NULL,'天使在身边angel','',850.00,NULL,NULL,NULL,NULL,-850.00,'','','未签收','静音键破损','admin'),
(41,'Z1005','2019-02-26','iphone8 plus64','       白色','国行',NULL,'xianjianmiling','803613764022',3120.00,NULL,NULL,NULL,NULL,-3120.00,'','','未签收','    盒子 充电器','zhangxiaodong'),
(42,'R1001','2019-02-26','iPhone7 128','黑色','国行',NULL,'嗯嗯啊啊哦哦额','',1200.00,NULL,NULL,NULL,NULL,-1200.00,'','','未签收','换过国产屏，电池70%左右','rendonghai'),
(43,'R1002','2019-02-26','iphone7 32',' 金色','国行',NULL,'qq632922983','',1250.00,NULL,NULL,NULL,NULL,-1250.00,'','','未签收','','rendonghai'),
(44,'R1003','2019-02-26','iPhone7 128','黑色','国行',NULL,'超级帅的zj','',1500.00,NULL,NULL,NULL,NULL,-1500.00,'','','未签收','','rendonghai'),
(45,'R1004','2019-02-26','iphone7 32',' 银色','国行',NULL,'ab15977546224','',1129.00,NULL,NULL,NULL,NULL,-1129.00,'','','未签收','走平台验机','rendonghai'),
(46,'Z1006','2019-02-26','iphone7plus 32','磨砂黑','国行',NULL,'max12324097','',1850.00,NULL,NULL,NULL,NULL,-1850.00,'','','未签收','发票 盒子 充电器 ','zhangxiaodong'),
(47,'L1004','2019-02-26','iphone8 64','金色','国行',NULL,'陈杰爱陈欣','',2450.00,NULL,NULL,NULL,NULL,-2450.00,'','','未签收','全原+耳机充电，细微划痕','luodachun'),
(48,'L1005','2019-02-26','iphone7 32','黑色','国行',NULL,'tb679722211','',1100.00,NULL,NULL,NULL,NULL,-1100.00,'','','未签收','国行全原，无拆无修，背板掉漆厉害，面板一条明显划痕','luodachun');

/*Table structure for table `persistent_logins` */

DROP TABLE IF EXISTS `persistent_logins`;

CREATE TABLE `persistent_logins` (
  `series` varchar(255) NOT NULL,
  `last_used` datetime DEFAULT NULL,
  `token` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`series`),
  UNIQUE KEY `UK_bqa9l0go97v49wwyx4c0u3kpd` (`token`),
  UNIQUE KEY `UK_f8t9fsfwc17s6qcbx0ath6l3h` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `persistent_logins` */

/*Table structure for table `product_apperance` */

DROP TABLE IF EXISTS `product_apperance`;

CREATE TABLE `product_apperance` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '外观id',
  `code` int(100) DEFAULT NULL COMMENT '外观编号',
  `name` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '外观类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='外观字典表';

/*Data for the table `product_apperance` */

insert  into `product_apperance`(`id`,`code`,`name`) values 
(7,1,'屏幕轻微划痕'),
(8,2,'屏幕严重划痕'),
(9,3,'外壳轻微磕碰掉漆'),
(10,4,'外壳严重磕碰掉漆');

/*Table structure for table `product_attachment` */

DROP TABLE IF EXISTS `product_attachment`;

CREATE TABLE `product_attachment` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '随机附件id',
  `code` int(100) DEFAULT NULL COMMENT '随机附件编号',
  `name` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '随机附件类别',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='附件字典表';

/*Data for the table `product_attachment` */

insert  into `product_attachment`(`id`,`code`,`name`) values 
(7,1,'数据线'),
(8,2,'充电器'),
(9,3,'耳机'),
(10,4,'箱说（盒子）');

/*Table structure for table `product_brand` */

DROP TABLE IF EXISTS `product_brand`;

CREATE TABLE `product_brand` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '品牌id',
  `code` int(100) DEFAULT NULL COMMENT '品牌编号',
  `name` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '品牌',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='品牌字典表';

/*Data for the table `product_brand` */

insert  into `product_brand`(`id`,`code`,`name`) values 
(94,1,'苹果'),
(95,2,'华为');

/*Table structure for table `product_color` */

DROP TABLE IF EXISTS `product_color`;

CREATE TABLE `product_color` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '颜色id',
  `code` int(100) DEFAULT NULL COMMENT '颜色编号',
  `name` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '颜色名字',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='颜色字典表';

/*Data for the table `product_color` */

insert  into `product_color`(`id`,`code`,`name`) values 
(7,1,'玫瑰金'),
(8,2,'土豪金'),
(9,3,'银色'),
(10,4,'磨砂黑'),
(11,5,'亮黑色'),
(12,6,'白色'),
(13,7,'红色'),
(14,8,'金色');

/*Table structure for table `product_guarantee` */

DROP TABLE IF EXISTS `product_guarantee`;

CREATE TABLE `product_guarantee` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '保修id',
  `code` int(100) DEFAULT NULL COMMENT '保修编号',
  `name` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '保修类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='保修字典表';

/*Data for the table `product_guarantee` */

insert  into `product_guarantee`(`id`,`code`,`name`) values 
(8,1,'在保'),
(9,2,'过保');

/*Table structure for table `product_info` */

DROP TABLE IF EXISTS `product_info`;

CREATE TABLE `product_info` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '商品类型id',
  `brand` int(100) DEFAULT NULL COMMENT '品牌编号',
  `model` int(100) DEFAULT NULL COMMENT '型号编号',
  `standard` int(100) DEFAULT NULL COMMENT '规格编号',
  `base_royalty` decimal(65,2) DEFAULT NULL COMMENT '采购基础提成',
  `sale_royalty` decimal(65,2) DEFAULT NULL COMMENT '采购额外提成',
  `scratch_price` decimal(65,2) DEFAULT NULL COMMENT '有轻微划痕价格',
  `knock_price` decimal(65,2) DEFAULT NULL COMMENT '有轻微磕碰价格',
  `packing_price` decimal(65,2) DEFAULT NULL COMMENT '有包装价格',
  `attachment_price` decimal(65,2) DEFAULT NULL COMMENT '有配件价格',
  `base_price` decimal(65,2) DEFAULT NULL COMMENT '基础价格',
  `scratch_pricetwo` decimal(65,2) DEFAULT NULL COMMENT '有严重划痕价格',
  `knock_pricetwo` decimal(65,2) DEFAULT NULL COMMENT '有严重磕碰价格',
  `nationlock` decimal(65,2) DEFAULT NULL COMMENT '有国行无锁的价格',
  `warranty` decimal(65,2) DEFAULT NULL COMMENT '在保修期的价格',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='商品字典表';

/*Data for the table `product_info` */

insert  into `product_info`(`id`,`brand`,`model`,`standard`,`base_royalty`,`sale_royalty`,`scratch_price`,`knock_price`,`packing_price`,`attachment_price`,`base_price`,`scratch_pricetwo`,`knock_pricetwo`,`nationlock`,`warranty`) values 
(89,1,1,1,15.00,0.30,0.00,0.00,0.00,0.00,550.00,50.00,50.00,100.00,0.00),
(90,1,1,2,15.00,0.30,0.00,0.00,0.00,0.00,600.00,50.00,50.00,100.00,0.00),
(91,1,1,3,15.00,0.30,0.00,0.00,0.00,0.00,700.00,50.00,50.00,100.00,0.00),
(92,1,2,1,15.00,0.30,0.00,0.00,0.00,0.00,800.00,50.00,50.00,100.00,0.00),
(93,1,2,2,15.00,0.30,0.00,0.00,0.00,0.00,850.00,50.00,50.00,100.00,0.00),
(94,1,2,3,15.00,0.30,0.00,0.00,0.00,0.00,1000.00,50.00,50.00,100.00,0.00),
(95,1,3,1,15.00,0.30,0.00,0.00,0.00,0.00,750.00,50.00,50.00,100.00,0.00),
(96,1,3,2,15.00,0.30,0.00,0.00,0.00,0.00,900.00,50.00,50.00,100.00,0.00),
(97,1,3,3,15.00,0.30,0.00,0.00,0.00,0.00,1080.00,50.00,50.00,100.00,0.00),
(98,1,4,1,15.00,0.30,0.00,0.00,0.00,0.00,1000.00,50.00,50.00,100.00,0.00),
(99,1,4,2,15.00,0.30,0.00,0.00,0.00,0.00,1200.00,50.00,50.00,100.00,0.00),
(100,1,4,3,15.00,0.30,0.00,0.00,0.00,0.00,1400.00,50.00,50.00,100.00,0.00),
(101,1,5,2,15.00,0.30,0.00,0.00,0.00,0.00,1400.00,50.00,100.00,100.00,0.00),
(102,1,5,4,15.00,0.30,0.00,0.00,0.00,0.00,1750.00,50.00,100.00,100.00,0.00),
(103,1,6,2,15.00,0.30,0.00,0.00,0.00,0.00,2050.00,50.00,100.00,100.00,0.00),
(104,1,6,4,15.00,0.30,0.00,0.00,0.00,0.00,2500.00,50.00,100.00,100.00,0.00),
(105,1,7,3,15.00,0.30,0.00,0.00,20.00,20.00,2550.00,50.00,100.00,150.00,0.00),
(106,1,7,5,15.00,0.30,0.00,0.00,20.00,20.00,2900.00,50.00,100.00,150.00,0.00),
(107,1,8,3,15.00,0.30,0.00,0.00,20.00,20.00,3300.00,50.00,100.00,150.00,0.00),
(108,1,8,5,15.00,0.30,0.00,0.00,20.00,20.00,3700.00,100.00,150.00,150.00,0.00),
(109,1,9,3,15.00,0.30,0.00,0.00,20.00,20.00,4300.00,200.00,150.00,200.00,0.00),
(110,1,9,5,15.00,0.30,0.00,0.00,20.00,20.00,4900.00,200.00,200.00,200.00,0.00);

/*Table structure for table `product_method` */

DROP TABLE IF EXISTS `product_method`;

CREATE TABLE `product_method` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '采购方法id',
  `code` int(100) DEFAULT NULL COMMENT '采购方法编号',
  `name` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '采购方法描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='采购方式';

/*Data for the table `product_method` */

insert  into `product_method`(`id`,`code`,`name`) values 
(8,1,'闲鱼'),
(9,2,'转转'),
(10,3,'线下或其他');

/*Table structure for table `product_model` */

DROP TABLE IF EXISTS `product_model`;

CREATE TABLE `product_model` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '型号id',
  `code` int(100) DEFAULT NULL COMMENT '型号编码',
  `name` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '型号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='模型字典表';

/*Data for the table `product_model` */

insert  into `product_model`(`id`,`code`,`name`) values 
(48,1,'iphone6'),
(49,2,'iphone6 Plus'),
(50,3,'iphone6s'),
(51,4,'iphone6s Plus'),
(52,5,'iphone7'),
(53,6,'iphone7 Plus'),
(54,7,'iphone8'),
(55,8,'iphone8 Plus'),
(56,9,'iphoneX'),
(57,10,'iphone XS'),
(58,11,'iphone XS MAX'),
(59,12,'iphone XR');

/*Table structure for table `product_repair` */

DROP TABLE IF EXISTS `product_repair`;

CREATE TABLE `product_repair` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '维修id',
  `code` int(100) DEFAULT NULL COMMENT '维修编号',
  `name` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '维修类型名',
  `price` decimal(65,2) DEFAULT NULL COMMENT '提成',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='维修字典表';

/*Data for the table `product_repair` */

insert  into `product_repair`(`id`,`code`,`name`,`price`) values 
(37,1,'更换外屏',1.00),
(38,2,'更换后盖',1.00),
(39,3,'更换电池',1.00);

/*Table structure for table `product_standard` */

DROP TABLE IF EXISTS `product_standard`;

CREATE TABLE `product_standard` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '规格id',
  `code` int(100) DEFAULT NULL COMMENT '规格编号',
  `name` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '规格名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='规格字典表';

/*Data for the table `product_standard` */

insert  into `product_standard`(`id`,`code`,`name`) values 
(19,1,'16g'),
(20,2,'32g'),
(21,3,'64g'),
(22,4,'128g'),
(23,5,'256g'),
(24,6,'512g');

/*Table structure for table `product_state` */

DROP TABLE IF EXISTS `product_state`;

CREATE TABLE `product_state` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '订单状态id',
  `code` int(100) DEFAULT NULL COMMENT '订单状态编号',
  `name` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '订单状态类别',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='订单状态表';

/*Data for the table `product_state` */

insert  into `product_state`(`id`,`code`,`name`) values 
(1,1,'待质检'),
(2,2,'待维修'),
(3,3,'良品'),
(4,4,'交易成功'),
(5,5,'退回'),
(6,6,'退款'),
(7,7,'部分退款'),
(8,8,'已售出'),
(9,9,'待确认'),
(10,10,'返厂'),
(11,0,'失效');

/*Table structure for table `quality_list` */

DROP TABLE IF EXISTS `quality_list`;

CREATE TABLE `quality_list` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '质检列表id',
  `quality_id` bigint(100) DEFAULT NULL COMMENT '质检人员id',
  `order_id` bigint(100) DEFAULT NULL COMMENT '订单id',
  `time` date DEFAULT NULL COMMENT '质检时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='质检人员订单记录表';

/*Data for the table `quality_list` */

insert  into `quality_list`(`id`,`quality_id`,`order_id`,`time`) values 
(144,1,197,'2019-02-13'),
(145,1,195,'2019-02-14'),
(146,1,198,'2019-02-14'),
(147,1,199,'2019-02-14'),
(148,1,200,'2019-02-14'),
(149,1,213,'2019-02-15'),
(150,1,203,'2019-02-15'),
(151,1,204,'2019-02-15'),
(152,1,210,'2019-02-15'),
(153,1,214,'2019-02-15'),
(154,1,201,'2019-02-15'),
(155,1,196,'2019-02-15'),
(156,1,202,'2019-02-16'),
(157,1,209,'2019-02-16'),
(158,1,208,'2019-02-16'),
(159,1,212,'2019-02-23');

/*Table structure for table `quality_royalty` */

DROP TABLE IF EXISTS `quality_royalty`;

CREATE TABLE `quality_royalty` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '标识id',
  `royalty` decimal(65,2) DEFAULT NULL COMMENT '质检提成',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='质检提成表';

/*Data for the table `quality_royalty` */

insert  into `quality_royalty`(`id`,`royalty`) values 
(1,10.00);

/*Table structure for table `refund_list` */

DROP TABLE IF EXISTS `refund_list`;

CREATE TABLE `refund_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(100) DEFAULT NULL COMMENT '填写退回信息的采购人员id',
  `logistics_num` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '退回物流单号',
  `logistics_company` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '退回的物流公司',
  `carriage_price` decimal(65,2) DEFAULT NULL COMMENT '退回的物流价格',
  `time` date DEFAULT NULL COMMENT '退回的价格',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='记录退回信息';

/*Data for the table `refund_list` */

/*Table structure for table `repair_list` */

DROP TABLE IF EXISTS `repair_list`;

CREATE TABLE `repair_list` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '维修列表记录id',
  `repair_id` bigint(100) DEFAULT NULL COMMENT '维修人员id',
  `order_id` bigint(100) DEFAULT NULL COMMENT '订单id',
  `repair_style` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '维修类型',
  `time` date DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='维修人员订单记录表';

/*Data for the table `repair_list` */

insert  into `repair_list`(`id`,`repair_id`,`order_id`,`repair_style`,`time`) values 
(50,1,200,'2','2019-02-14');

/*Table structure for table `repair_management` */

DROP TABLE IF EXISTS `repair_management`;

CREATE TABLE `repair_management` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '维修管理id',
  `user_id` bigint(100) DEFAULT NULL COMMENT '维修人员id',
  `purchase_id` bigint(100) DEFAULT NULL COMMENT '订单id',
  `materiel_id` int(100) DEFAULT NULL COMMENT '物料编号',
  `select_style` int(100) DEFAULT NULL COMMENT '维修类型编码',
  `materiel_num` int(100) DEFAULT NULL COMMENT '数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='维修管理表';

/*Data for the table `repair_management` */

insert  into `repair_management`(`id`,`user_id`,`purchase_id`,`materiel_id`,`select_style`,`materiel_num`) values 
(73,1,200,1003,NULL,1);

/*Table structure for table `sold_out` */

DROP TABLE IF EXISTS `sold_out`;

CREATE TABLE `sold_out` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT COMMENT '已售出id',
  `user_id` bigint(100) DEFAULT NULL COMMENT '销售人员id',
  `purchase_id` bigint(100) DEFAULT NULL COMMENT '采购id',
  `logistics_num` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '物流单号',
  `logistics_company` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '物流公司',
  `carriage_price` decimal(65,2) DEFAULT NULL COMMENT '物流价格',
  `sales_person` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL COMMENT '销售员',
  `time` date DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci COMMENT='售出表';

/*Data for the table `sold_out` */

insert  into `sold_out`(`id`,`user_id`,`purchase_id`,`logistics_num`,`logistics_company`,`carriage_price`,`sales_person`,`time`) values 
(116,1,198,'446381060751','顺丰',12.00,'admin','2019-02-14'),
(117,1,200,'446381060779','顺丰',12.00,'admin','2019-02-15'),
(118,1,210,'290447728642','顺丰',12.00,'admin','2019-02-15'),
(119,1,203,'446381060788','顺丰',12.00,'admin','2019-02-15'),
(120,1,214,'446381060797','顺丰',12.00,'admin','2019-02-15'),
(121,1,201,'0','0',0.00,'admin','2019-02-16'),
(122,1,202,'0','顺丰',12.00,'admin','2019-02-16'),
(123,1,208,'0','顺丰',12.00,'admin','2019-02-17'),
(124,1,209,'1','1',18.00,'admin','2019-02-23'),
(125,1,195,'0','0',18.00,'admin','2019-02-23'),
(126,1,196,'0','0',18.00,'admin','2019-02-23'),
(127,1,204,'0','0',12.00,'admin','2019-02-24');

/*Table structure for table `sys_organization` */

DROP TABLE IF EXISTS `sys_organization`;

CREATE TABLE `sys_organization` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `available` bit(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKmeds2u6ae5usj0ko0bqj3k0eo` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_organization` */

/*Table structure for table `sys_resource` */

DROP TABLE IF EXISTS `sys_resource`;

CREATE TABLE `sys_resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `available` bit(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `order_num` int(11) NOT NULL,
  `permission` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3fekum3ead5klp7y4lckn5ohi` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8;

/*Data for the table `sys_resource` */

insert  into `sys_resource`(`id`,`available`,`name`,`order_num`,`permission`,`type`,`url`,`parent_id`) values 
(1,'\0','顶级栏目',100,NULL,0,NULL,0),
(2,'\0','权限配置',8,'',0,'',1),
(3,'\0','角色管理',102,'/role',0,'/role',2),
(4,'\0','权限管理',103,'/resource',0,'/resource',2),
(5,'\0','用户管理',101,'/user',0,'/user',2),
(6,'\0','编辑',100,'/role/editor-role',1,'/role/editor-role',3),
(7,'\0','添加权限节点',100,'/resource/add-permission',1,'/resource/add-permission',4),
(8,'\0','添加管理员',100,'/user/add-user',1,'/user/add-user',5),
(9,'\0','添加角色',100,'/role/add-role',1,'/role/add-role',3),
(10,'\0','删除角色',100,'/role/delete',1,'/role/delete',3),
(11,'\0','删除用户',100,'/user/delete-user',1,'/user/delete-user',5),
(12,'\0','删除权限',100,'/resource/delete',1,'/resource/delete',4),
(13,'\0','启用',100,'/user/available-user',1,'/user/available-user',3),
(14,'\0','修改管理员密码',100,'/user/modify-password',1,'/user/modify-password',5),
(16,NULL,'权限编辑',100,'/resource/editor-permission',1,'/resource/editor-permission',4),
(150,'\0','编辑管理员信息',100,'/user/edit-user',1,'/user/edit-user',5),
(165,NULL,'基础信息',7,'',0,'',1),
(167,NULL,'品牌管理',201,'/brand/selectbrand',0,'/brand/selectbrand',165),
(168,NULL,'型号管理',202,'/model/selectmodel',0,'/model/selectmodel',165),
(169,NULL,'维修类别',203,'/productRepair',0,'/productRepair',165),
(172,NULL,'商品管理',204,'/products',0,'/products',165),
(174,NULL,'精准报价',0,'',0,'',1),
(175,NULL,'精准报价',101,'/quoted',0,'/quoted',174),
(177,NULL,'规格管理',205,'/standard/selectstandard',0,'/standard/selectstandard',165),
(178,NULL,'保修管理',206,'/guarantee/selectguarantee',0,'/guarantee/selectguarantee',165),
(179,NULL,'颜色管理',207,'/color/selectcolor',0,'/color/selectcolor',165),
(180,NULL,'采购管理',1,'',0,'',1),
(181,NULL,'采购列表',100,'/purchase',0,'/purchase',180),
(182,NULL,'配件管理',208,'/attachment/selectattachment',0,'/attachment/selectattachment',165),
(183,NULL,'外观管理',209,'/apperance/selectapperance',0,'/apperance/selectapperance',165),
(184,NULL,'质检列表',100,'/quality',0,'/quality',185),
(185,NULL,'质检管理',2,'',0,'',1),
(186,NULL,'销售管理',4,'',0,'',1),
(187,NULL,'销售列表',100,'/sales',0,'/sales',186),
(190,NULL,'维修管理',3,NULL,0,NULL,1),
(191,NULL,'维修列表',100,'/repair',0,'/repair',190),
(194,NULL,'统计分析',6,'/royalty/caculate',0,'/royalty/caculate',1),
(195,NULL,'统计分析',100,'/royalty/caculate',0,'/royalty/caculate',194),
(196,NULL,'物料管理',5,'/materiel/find',0,'/materiel/find',1),
(197,NULL,'物料列表',100,'/materiel/find',1,'/materiel/find',196),
(198,NULL,'采购管理',11,'/dicmethod/selectdicmethod',0,'/dicmethod/selectdicmethod',165),
(199,NULL,'订单管理',6,'',0,'',1),
(200,NULL,'订单列表',10,'/order',0,'/order',199),
(201,NULL,'交易管理',20,'/orderlist',0,'/orderlist',1),
(202,NULL,'交易列表',10,'/orderlist',0,'/orderlist',201);

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `available` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

/*Data for the table `sys_role` */

insert  into `sys_role`(`id`,`available`,`description`,`name`) values 
(1,NULL,'管理员','管理员'),
(30,NULL,'','销售人员'),
(31,NULL,'','维修人员'),
(32,NULL,'','质检人员'),
(33,NULL,'采购人员','采购人员');

/*Table structure for table `sys_role_resources` */

DROP TABLE IF EXISTS `sys_role_resources`;

CREATE TABLE `sys_role_resources` (
  `sys_role_id` bigint(20) NOT NULL,
  `resources_id` bigint(20) NOT NULL,
  KEY `FKog6jj4v6yh9e1ilxk2mwuk75a` (`resources_id`),
  KEY `FKsqkqfd2hpr5cc2kbrtgoced2w` (`sys_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_role_resources` */

insert  into `sys_role_resources`(`sys_role_id`,`resources_id`) values 
(31,174),
(31,175),
(31,190),
(31,191),
(32,174),
(32,175),
(32,185),
(32,184),
(33,174),
(33,175),
(33,180),
(33,181),
(33,199),
(33,200),
(1,2),
(1,3),
(1,6),
(1,9),
(1,10),
(1,13),
(1,4),
(1,7),
(1,12),
(1,16),
(1,5),
(1,8),
(1,11),
(1,14),
(1,150),
(1,165),
(1,167),
(1,168),
(1,169),
(1,172),
(1,177),
(1,178),
(1,179),
(1,182),
(1,183),
(1,198),
(1,174),
(1,175),
(1,180),
(1,181),
(1,185),
(1,184),
(1,186),
(1,187),
(1,190),
(1,191),
(1,194),
(1,195),
(1,196),
(1,197),
(1,199),
(1,200),
(1,201),
(1,202),
(30,174),
(30,175),
(30,180),
(30,181),
(30,186),
(30,187),
(30,202);

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createtime` datetime DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `available` bit(1) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `sex_type` int(11) DEFAULT NULL COMMENT '性别(0.男,1.女)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

/*Data for the table `sys_user` */

insert  into `sys_user`(`id`,`createtime`,`password`,`updatetime`,`username`,`available`,`email`,`tel`,`sex_type`) values 
(1,'2017-07-11 17:42:18','$2a$10$SIU57gfkh/TsIVYALXBNAeDnQzkm652FT9cg4h8wtEfC306uliyYa','2019-01-11 07:34:38','admin','','1191134106@qq.com','15030103078',1),
(23,'2019-01-10 13:18:55','$2a$10$Q74mtX6pmKerfSyEeJtP8etAEd2qDLGWUzpz5UGECtn09tumLNFWC','2019-01-11 13:08:29','admin1','\0','','',0),
(25,'2019-01-14 10:58:47','$2a$10$SCxSgUt5sjpyuOGNipn/iOx5d1hoC7x1JshHgQJNcAC/hXyxBTcCq','2019-01-14 10:58:47','111111','\0','','',1),
(26,'2019-01-15 02:55:42','$2a$10$MVEujfGg26MhmDOE5oyfLeCUCxQBPFBhW6K83Ivh/onr51U6ArlzS','2019-01-15 02:57:26','saler','\0','','1111111',1),
(27,'2019-01-15 03:06:10','$2a$10$HbO1yi/8wIY0AI7czy6FeuBu/F.n9IqHyymU/w1oLkam0i8q8GPQO','2019-01-15 03:06:10','check','\0','','',1),
(28,'2019-01-15 03:06:43','$2a$10$XaYbI0VowitWrfHK012TheLlnoAmEzWycmn7O6p2ek66TK8YZWKq.','2019-01-15 03:06:43','repair','\0','','',1),
(30,'2019-02-11 09:26:24','$2a$10$WY2ukXIT9WGpIdEiPNuBr..FCB4Bj9SpF/q3E1NEb/R6xtcQyazQG','2019-02-25 09:32:39','zhujixing','','','',1),
(31,'2019-02-26 09:01:04','$2a$10$o8JEV6oHwZ/N6yr7NfXhJuqzXtt8tC824pcgSDhJDX4Z5KorvOB.S','2019-02-26 09:01:04','luodachun','','','',1),
(32,'2019-02-26 09:01:19','$2a$10$U/Fg57Rx8M9OXLG.nZbbruMvklKcEhmjFNwWg6.okPre/SPGg.HTm','2019-02-26 09:01:19','zhangxiaodong','','','',1),
(33,'2019-02-26 09:01:45','$2a$10$JSEIqHpdD4267DqbekyLw.hNMhPHIOBudWhgQmj4R/KbKkMwfIHFy','2019-02-26 09:01:45','rendonghai','','','',1);

/*Table structure for table `sys_user_roles` */

DROP TABLE IF EXISTS `sys_user_roles`;

CREATE TABLE `sys_user_roles` (
  `sys_user_id` bigint(20) NOT NULL,
  `roles_id` bigint(20) NOT NULL,
  KEY `FKdpvc6d7xqpqr43dfuk1s27cqh` (`roles_id`),
  KEY `FKd0ut7sloes191bygyf7a3pk52` (`sys_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_user_roles` */

insert  into `sys_user_roles`(`sys_user_id`,`roles_id`) values 
(1,1),
(25,1),
(26,30),
(27,32),
(28,31),
(30,1),
(31,1),
(32,1),
(33,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
