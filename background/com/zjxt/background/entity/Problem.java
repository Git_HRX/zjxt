package com.zjxt.background.entity;

public class Problem {
    private Integer id;

    private Integer unitNum;

    private Integer proNum;

    private String proName;

    private String proDes;

    private String proDeal;

    private Byte proYvaluehi;

    private Byte proYvaluelo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUnitNum() {
        return unitNum;
    }

    public void setUnitNum(Integer unitNum) {
        this.unitNum = unitNum;
    }

    public Integer getProNum() {
        return proNum;
    }

    public void setProNum(Integer proNum) {
        this.proNum = proNum;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName == null ? null : proName.trim();
    }

    public String getProDes() {
        return proDes;
    }

    public void setProDes(String proDes) {
        this.proDes = proDes == null ? null : proDes.trim();
    }

    public String getProDeal() {
        return proDeal;
    }

    public void setProDeal(String proDeal) {
        this.proDeal = proDeal == null ? null : proDeal.trim();
    }

    public Byte getProYvaluehi() {
        return proYvaluehi;
    }

    public void setProYvaluehi(Byte proYvaluehi) {
        this.proYvaluehi = proYvaluehi;
    }

    public Byte getProYvaluelo() {
        return proYvaluelo;
    }

    public void setProYvaluelo(Byte proYvaluelo) {
        this.proYvaluelo = proYvaluelo;
    }
}