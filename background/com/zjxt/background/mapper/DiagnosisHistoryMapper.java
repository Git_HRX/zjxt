package com.zjxt.background.mapper;

import com.zjxt.background.entity.DiagnosisHistory;

public interface DiagnosisHistoryMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DiagnosisHistory record);

    int insertSelective(DiagnosisHistory record);

    DiagnosisHistory selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DiagnosisHistory record);

    int updateByPrimaryKey(DiagnosisHistory record);
}